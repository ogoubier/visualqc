Plot Power Spectral Densities
=============================

Probabilistic Power Spectral Densities will be computed for each channel of a station. The results of the computation will be plotted and saved in a npz file. See the section :ref:`Plot PPSD station channel<plotPPSDSC>` for more explanation.

The median of Power Spectral Densities will then computed using npz files for each channel. The section :ref:`Plot Median PSD of a channel<plotPPSDC>` 
  

.. _plotPPSDSC:

Plot Probabilistic Power Spectral Densities (PPSD)
--------------------------------------------------

Provide graphical representation of the ppsd of a station.
To get more information for this command:

.. code-block:: console

   $plotPPSDSC -h

To run the command: 

.. code-block:: console

   $plotPPSDSC SDSDirectory stationXMLFilename --station stationCode --channel channelCode --startTime  "YYYY-MM-DDTHH:MM:SS" --endTime "YYYY-MM-DDTHH:MM:SS" [Options]

The plotPPSDSC command can be run with optional arguments:

.. code-block:: console

   $plotPPSDSC -h
   usage: plotPPSDSC [-h] [--station STACOD] [--channel CHACOD]
                  [--location LOCCOD] [--output OUTPUT] [--outFormat FMTID]
                  [--npzDir NPZDIR] [--npzFileName NPZFILENAME]
                  [--result RESULT] --startTime START_TIME --endTime END_TIME
                  [--duration SECONDCOUNT] [--isHydroOct]
                  INPUTDIR INPUTMETAFILE

   Provide graphical representation of the ppsd of a station (fournit diagramme
   probabiliste des Densités Spectrales de Puissance pour chaque canal, de code
   distinct, parmi toutes les stations)

   positional arguments:
     INPUTDIR              <INPUTDIR> input SDS root directory path
     INPUTMETAFILE         <INPUTMETAFILE> input file, wildcards accepted (please
                           write the name with wildcards * between quotes, ex.
                           "*.xml")

   optional arguments:
     -h, --help            show this help message and exit
     --station STACOD      <STACOD> Optional station code
     --channel CHACOD      <CHACOD> Optional channel code
     --location LOCCOD     <LOCCOD> Optional location code
     --output OUTPUT       <OUTPUT> Optional output file name
     --outFormat FMTID     <FMTID> Optional format of output file (JPEG ro PNG)
     --npzDir NPZDIR       <NPZDIR> Optional directory for npz output files
     --npzFileName NPZFILENAME
                           <NPZFILE> Optional file name for npz output
     --result RESULT       <RESULT> Required path to a CSV output file
     --startTime START_TIME
                           <START_TIME> Required starting time
     --endTime END_TIME    <END_TIME> Optional end time
     --duration SECONDCOUNT
                           <SECONDCOUNT> Optional number of second for each
                           station waveform plot, this usually corresponds to the
                           duration of a seismic event
     --isHydroOct


.. _plotPPSDC:

Plot Median Power Spectral Densities (Median PSD)
-------------------------------------------------

Provide graphical representation of the median psd of a channel for all stations. The median PSD will be computed using npz files.
To get help for this command:

.. code-block:: console

   $plotPPSDC -h

To run the command:

.. code-block:: console

   $plotPPSDC stationXMLFilename channelCode [Options]

The plotPPSDC command can be run with optional arguments:

.. code-block:: console

   $plotPPSDC -h
   usage: plotPPSDC [-h] [--output OUTPUT] [--outFormat FMTID] [--npzDir NPZDIR]
                 [--result RESULT]
                 INPUTMETAFILE CHANNELCODE

   Provide graphical representation of the ppsd of a channel for all stations
   (fournit diagramme probabiliste des Densités Spectrales de Puissance pour
   chaque canal, de code distinct, parmi toutes les stations)

   positional arguments:
     INPUTMETAFILE      <INPUTMETAFILE> input file, wildcards accepted (please
                        write the name with wildcards * between quotes, ex.
                        "*.xml")
     CHANNELCODE        <CHANNELCODE> channel code required

   optional arguments:
     -h, --help         show this help message and exit
     --output OUTPUT    <OUTPUTFILE> Optional output file name
     --outFormat FMTID  <FMTID> Optional format of output file (JPEG ro PNG)
     --npzDir NPZDIR    <NPZDIR> Optional directory for npz input files
     --result RESULT    <RESULT> Required path to a CSV output file
