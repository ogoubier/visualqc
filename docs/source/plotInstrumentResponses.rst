Plot Instrument Responses
=========================

There are 2 kinds of plots for instrument responses:

* :ref:`Plot instrument responses for a station<plotIRS>` 
  
* :ref:`Plot instrument responses for a channel<plotIRC>` 
  

.. _plotIRS:

Plot instrument responses for a station
---------------------------------------

Create a plot instrument responses of a station with all its channels
To get more information about this plot command, type the following

.. code-block:: console

   $plotInstrumentResponseS -h

To create an image representing the responses of instruments used for a station, type:

.. code-block:: console

   $plotInstrumentResponseS [Options] stationXMLfilename --station stationCode

The use of the plotInstrumentResponseS command with all optional arguments:

.. code-block:: console

   $ plotInstrumentResponseS -h
   usage: plotInstrumentResponseS [-h] [--output OUTPUTFILE] --station STACOD
                               [--outFormat FMTID] [--result RESULT]
                               INPUTMETAFILE

   Provide plots of instrument response with all channels for one station
   (fournit graphe/courbe de réponse instrumentale, avec tous les canaux, pour
   une station)

   positional arguments:
     INPUTMETAFILE        <INPUTMETAFILE> input file, wildcards accepted (please
                          write the name with wildcards * between quotes, ex.
                          "*.xml")

   optional arguments:
     -h, --help           show this help message and exit
     --output OUTPUTFILE  <OUTPUTFILE> Optional output file name
     --station STACOD     <STACOD> Optional code stationn required when there are
                          more than one stationXML file
     --outFormat FMTID    <FMTID> Optional format of output file (JPEG ro PNG)
     --result RESULT      <RESULT> Required path to a CSV output file

.. _plotIRC:

Plot instrument responses for a channel
---------------------------------------

Create a plot instrument response of a channel for all stations possessing this channel
Type the following command to get more explanation

.. code-block:: console

   $plotInstrumentResponseC -h

To create an image representing the responses of instruments used for a channel, type:

.. code-block:: console

   $plotInstrumentResponseC [Options] stationXMLfilename --channel channelCode

The use of the plotInstrumentResponseC command with all optional arguments:

.. code-block:: console

   $ plotInstrumentResponseC -h
   usage: plotInstrumentResponseC [-h] --channel CHACOD [--output OUTPUT]
                               [--outFormat FMTID] [--result RESULT]
                               [--isHydroOct]
                               INPUTMETAFILE

   Provide plot of instrument response of a channel for all stations (fournit
   graphe/courbe de réponse instrumentale pour chaque canal, de code distinct,
   parmi toutes les stations)

   positional arguments:
     INPUTMETAFILE      <INPUTMETAFILE> input file, wildcards accepted (please
                        write the name with wildcards * between quotes, ex.
                        "*.xml")

   optional arguments:
     -h, --help         show this help message and exit
     --channel CHACOD   <CHACOD> Required channel code
     --output OUTPUT    <OUTPUT> Optional output file name
     --outFormat FMTID  <FMTID> Optional format of output file (JPEG ro PNG)
     --result RESULT    <RESULT> Required path to a CSV output file
     --isHydroOct
